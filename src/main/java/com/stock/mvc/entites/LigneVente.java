package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class LigneVente  implements Serializable {
	@Id
	@GeneratedValue
	private long idLigneVente;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article articles;
	
	@ManyToOne
	@JoinColumn(name = "idVente")
	private Vente lignesventes;
	
	public Article getArticles() {
		return articles;
	}

	public void setArticles(Article articles) {
		this.articles = articles;
	}

	public long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(long id) {
		this.idLigneVente = id;
	}
}
