package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class LigneCommandeFournisseur  implements Serializable {
	@Id
	@GeneratedValue
	private long idLigneCommandeFournisseur;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article articles;
	@ManyToOne
	@JoinColumn(name = "idcommandefournisseur")
	private CommandeFournisseur commandesfournisseurs;

	public long getIdLigneCommandeFournisseur() {
		return idLigneCommandeFournisseur;
	}

	public void setIdLigneCommandeFournisseur(long id) {
		this.idLigneCommandeFournisseur = id;
	}
}
