package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class CommandeClient  implements Serializable {
	@Id
	@GeneratedValue
	private long idCommandeClient;
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommande;
	@ManyToOne
	@JoinColumn(name = "idClient")
	private Client client;
	@OneToMany(mappedBy = "commandesclients")
	private List<LigneCommandeClient> LigneCommandeClients;
	
	

	public long getIdCommandeClient() {
		return idCommandeClient;
	}

	public void setIdCommandeClient(long id) {
		this.idCommandeClient = id;
	}
}
