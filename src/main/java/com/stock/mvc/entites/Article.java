package com.stock.mvc.entites;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Article implements Serializable {
	@Id
	@GeneratedValue
	private long idArticle;
	private String codeArticle;
	private String designation;
	private BigDecimal prixunitaireHT;
	private BigDecimal tauxTVA;
	private BigDecimal prixunitaireTTC;
	private String photo;
	@ManyToOne
	@JoinColumn(name = "idcategory")
	private Category category;
	
	public Article() {
		
	}
	
	public long getIdArticle() {
		return idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixunitaireHT() {
		return prixunitaireHT;
	}

	public void setPrixunitaireHT(BigDecimal prixunitaireHT) {
		this.prixunitaireHT = prixunitaireHT;
	}

	public BigDecimal getTauxTVA() {
		return tauxTVA;
	}

	public void setTauxTVA(BigDecimal tauxTVA) {
		this.tauxTVA = tauxTVA;
	}

	public BigDecimal getPrixunitaireTTC() {
		return prixunitaireTTC;
	}

	public void setPrixunitaireTTC(BigDecimal prixunitaireTTC) {
		this.prixunitaireTTC = prixunitaireTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setIdArticle(long idArticle) {
		this.idArticle = idArticle;
	}
	
}
