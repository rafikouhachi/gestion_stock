package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Vente  implements Serializable {
	@Id
	@GeneratedValue
	private long idVente;
	
	private String code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datevente;
	
	@OneToMany(mappedBy = "lignesventes")
	private List<LigneVente> ligneventes;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDatevente() {
		return datevente;
	}

	public void setDatevente(Date datevente) {
		this.datevente = datevente;
	}

	public List<LigneVente> getLignesventes() {
		return ligneventes;
	}

	public void setLignesventes(List<LigneVente> lignesventes) {
		this.ligneventes = lignesventes;
	}

	public long getIdVente() {
		return idVente;
	}

	public void setIdVente(long id) {
		this.idVente = id;
	}
}
