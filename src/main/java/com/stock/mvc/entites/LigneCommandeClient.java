package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class LigneCommandeClient  implements Serializable {
	@Id
	@GeneratedValue
	private long idlignecommandeclient;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article articles;
	@ManyToOne
	@JoinColumn(name = "idcommandeclient")
	private CommandeClient commandesclients;

	public Article getArticles() {
		return articles;
	}

	public void setArticles(Article articles) {
		this.articles = articles;
	}

	public CommandeClient getCommandesclients() {
		return commandesclients;
	}

	public void setCommandesclients(CommandeClient commandesclients) {
		this.commandesclients = commandesclients;
	}

	public long getIdlignecommandeclient() {
		return idlignecommandeclient;
	}

	public void setIdlignecommandeclient(long id) {
		this.idlignecommandeclient = id;
	}
}
